#!/bin/sh

mkdir -p compressed

for f in ./*.mp3; do
	ffmpeg -i "$f" -map 0:a:0 -b:a 128k "compressed/$f"
done

