import sys
import os

print("building /f/comic/index.html...")

template_outer = '''
<!DOCTYPE html>

<html lang="en">
	<head>
		<title>
			512b // fromic
		</title>
		<link type="text/css" rel="stylesheet" href="/assets/style.css" />
	</head>
	<body style="background-color:#324;">
		<div id="a">
			<div id="b">
				<div class="content">
					<h1>
						fromic
					</h1>
					{0}
				</div>
			</div>
		</div>
	</body>
</html>
'''.strip()
template_inner = '<a href="/mjm/comic/{0}">{1}</a>'
separator = "\n\t\t\t\t\t/\n\t\t\t\t\t"

result=[]

i = 1
for f in next(os.walk('./mjm/comic'))[2]:
	if f != "index.html" and f.endswith(".html"):
		result.append(template_inner.format(f, i))
		i += 1

result = template_outer.format(separator.join(result))

with open('./mjm/comic/index.html', 'w') as f:
	f.write(result)

print("finish")
