import sys
import os

print("building /a-z.html...")

template_outer = '''
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
	<head>
		<title>
			512b // a-z
		</title>
		<link type="text/css" rel="stylesheet" href="/assets/style.css" />
	</head>
	<body>
		<div id="a">
			<div id="b">
				<div class="content">
					<h1>
						a-z
					</h1>
					<a href="/">512b</a>
{0}				</div>
			</div>
		</div>
	</body>
</html>
'''.strip()
template_inner = '\t\t\t\t\t<a href="/{0}">{0}</a>\n'

try:
	with open('.az-ignore') as f:
		ignore = [
			l.strip()
			for l in f.read().strip().split('\n')
			if len(l.strip())>0 and not l.strip().startswith('#')
		]
except:
	print('.az-ignore not found in directory')
	sys.exit(1)

result=''

for i in next(os.walk('.'))[1]:
	if i not in ignore:
		result += template_inner.format(i)

result = template_outer.format(result)

with open('./a-z.html', 'w') as f:
	f.write(result)

print("finish")
