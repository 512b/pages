#ifdef WEB
/* todo */
#else
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

unsigned char agetchar(void) {
  unsigned char c = 0;
  read(0, &c, 1);
  if (c == 0177)
    c = '\b';
  return c;
}

void aputchar(unsigned int c) {
  write(1, &c, 1);
  if (c == '\b')
    write(1, " \x08", 3);
}
#endif

static struct {
  unsigned int r[8];
  unsigned int z, c;
} machine;

unsigned char ram[0177777];

unsigned char read8(unsigned int addr) {
  if (addr < 0177776)
    return ram[addr];
  else if (addr == 0177776)
    return agetchar();
  else
    return 0;
}

void write8(unsigned int addr, unsigned char val) {
  if (addr < 0177776)
    ram[addr] = val;
  else if (addr == 0177776)
    aputchar(val);
}

unsigned int read16(unsigned int addr) {
  return ((unsigned int) read8(addr)) | ((unsigned int) read8(addr + 1) << 8);
}

void write16(unsigned int addr, unsigned int val) {
  write8(addr, (unsigned char) val);
  write8(addr + 1, (unsigned char) (val >> 8));
}

enum { OUT_OF_PLACE, IN_PLACE };

unsigned int reada(unsigned int src, unsigned int placedness) {
  unsigned int mode, reg;
  mode = (src >> 3) & 3;
  reg = src & 7;
  switch (mode) {
  case 0: return machine.r[reg];
  case 1: return read16(machine.r[reg]);
  case 2: {
    unsigned int res;
    res = read16(machine.r[reg]);
    if (placedness == OUT_OF_PLACE) {
      machine.r[reg] += 2;
      machine.r[reg] &= 0177777U;
    }
    return res;
  }
  case 3:
    if (placedness == OUT_OF_PLACE) {
      machine.r[reg] -= 2;
      machine.r[reg] &= 0177777U;
      return read16(machine.r[reg]);
    } else {
      return read16(machine.r[reg] - 2);
    }
  }
}

void writea(unsigned int dst, unsigned int val) {
  unsigned int mode, reg;
  mode = (dst >> 3) & 3;
  reg = dst & 7;
  switch (mode) {
  case 0: machine.r[reg] = val; break;
  case 1: write16(machine.r[reg], val); break;
  case 2:
    write16(machine.r[reg], val);
    machine.r[reg] += 2;
    machine.r[reg] &= 0177777U;
    break;
  case 3:
    machine.r[reg] -= 2;
    machine.r[reg] &= 0177777U;
    write16(machine.r[reg], val);
    break;
  }
}

void step(void) {
  unsigned int inst, dst, src, res, lhs, rhs;
  inst = read8(machine.r[7]++);
  machine.r[7] &= 0177777U;
  if ((inst & 0300) != 0300) {
    dst = inst & 037;
    switch (inst >> 5) {
    case 0: /* inc */
      res = reada(dst, IN_PLACE);
      if (res == 0177777U) {
        writea(dst, 0);
        machine.z = 1;
        machine.c = 1;
      } else {
        writea(dst, res + 1);
        machine.z = 0;
        machine.c = 0;
      }
      return;
    case 1: /* dec */
      res = reada(dst, IN_PLACE);
      if (res == 0) {
        writea(dst, 0177777U);
        machine.z = 0;
        machine.c = 1;
      } else {
        writea(dst, res - 1);
        machine.z = res == 1;
        machine.c = 0;
      }
      return;
    case 2: /* not */
      res = reada(dst, IN_PLACE);
      writea(dst, res ^ 0177777U);
      machine.z = res == 0177777U;
      machine.c = 0;
      return;
    case 3: /* rsh */
      res = reada(dst, IN_PLACE);
      writea(dst, res >> 1);
      machine.z = res <= 1;
      machine.c = 0;
      return;
    case 4: /* push */
      writea(036, reada(dst, OUT_OF_PLACE));
      return;
    case 5: /* pop */
      writea(dst, reada(027, OUT_OF_PLACE));
      return;
    }
  } else {
    inst <<= 8;
    inst |= read8(machine.r[7]++);
    machine.r[7] &= 0177777U;
    dst = (inst >> 5) & 037;
    src = inst & 037;
    switch (inst >> 10) {
    case 060: /* mov */
      writea(dst, reada(src, OUT_OF_PLACE));
      return;
    case 061: /* cmoveq */
      if (machine.z)
        writea(dst, reada(src, OUT_OF_PLACE));
      else
        (void) reada(src, OUT_OF_PLACE);
      return;
    case 062: /* cmovne */
      if (!machine.z)
        writea(dst, reada(src, OUT_OF_PLACE));
      else
        (void) reada(src, OUT_OF_PLACE);
      return;
    case 063: /* cmovlt */
      if (machine.c)
        writea(dst, reada(src, OUT_OF_PLACE));
      else
        (void) reada(src, OUT_OF_PLACE);
      return;
    case 064: /* cmovgt */
      if (!machine.c && !machine.z)
        writea(dst, reada(src, OUT_OF_PLACE));
      else
        (void) reada(src, OUT_OF_PLACE);
      return;
    case 065: /* call */
      res = reada(src, OUT_OF_PLACE);
      writea(dst, machine.r[7]);
      machine.r[7] = res;
      return;
    case 066: /* cmp */
      lhs = reada(src, OUT_OF_PLACE);
      rhs = reada(dst, OUT_OF_PLACE);
      machine.c = lhs < rhs;
      machine.z = lhs == rhs;
      return;
    case 067: /* add */
      machine.c = 0;
      rhs = reada(src, OUT_OF_PLACE);
      lhs = reada(dst, IN_PLACE);
      res = (lhs + rhs + machine.c) & 0177777;
      if (!machine.c)
        machine.c = (lhs > 0177777 - rhs) ||
                    (rhs > 0177777 - lhs);
      else
        machine.c = (lhs == 0177777) ||
                    (rhs == 0177777) ||
                    (lhs > 0177776 - rhs) ||
                    (rhs > 0177776 - lhs);
      machine.z = res == 0;
      writea(dst, res);
      return;
    case 070: /* sub */
      machine.c = 0;
      rhs = reada(src, OUT_OF_PLACE);
      lhs = reada(dst, IN_PLACE);
      rhs = (rhs ^ 0177777) + !machine.c & 0177777;
      res = (lhs + rhs + machine.c) & 0177777;
      if (!machine.c)
        machine.c = (lhs > 0177777 - rhs) ||
                    (rhs > 0177777 - lhs);
      else
        machine.c = (lhs == 0177777) ||
                    (rhs == 0177777) ||
                    (lhs > 0177776 - rhs) ||
                    (rhs > 0177776 - lhs);
      machine.z = res == 0;
      writea(dst, res);
      return;
    case 071: /* and */
      rhs = reada(src, OUT_OF_PLACE);
      lhs = reada(dst, IN_PLACE);
      res = lhs & rhs;
      machine.c = 0;
      machine.z = res == 0;
      writea(dst, res);
      return;
    case 072: /* or */
      rhs = reada(src, OUT_OF_PLACE);
      lhs = reada(dst, IN_PLACE);
      res = lhs | rhs;
      machine.c = 0;
      machine.z = res == 0;
      writea(dst, res);
      return;
    case 073: /* xor */
      rhs = reada(src, OUT_OF_PLACE);
      lhs = reada(dst, IN_PLACE);
      res = lhs ^ rhs;
      machine.c = 0;
      machine.z = res == 0;
      writea(dst, res);
      return;
    case 074: /* setcc */
      if (src & 4)
        res = (src & 2 ? machine.c : machine.z) ^ (src & 1);
      else
        res = (machine.z == (src >> 1 & 1)) & (machine.c == (src & 1));
      if (src & 8)
        res = res ? 01777776 : 0;
      writea(dst, res);
      return;
    case 077: /* dbg */
      fprintf(stderr, "[DEBUG #%.4o\n", inst & 01777);
      fprintf(stderr, "  R0=%.6o R1=%.6o R2=%.6o R3=%.6o\n", machine.r[0], machine.r[1], machine.r[2], machine.r[3]);
      fprintf(stderr, "  R4=%.6o R5=%.6o R6=%.6o R7=%.6o]\n\n", machine.r[4], machine.r[5], machine.r[6], machine.r[7]);
      return; //*/
    }
  }
}

#ifndef WEB
int main(void) {
  struct termios term;
  setvbuf(stdin, (char *) 0, _IONBF, 0);
  setvbuf(stdout, (char *) 0, _IONBF, 0);
  tcgetattr(0, &term);
  term.c_lflag &= ~ICANON & ~ECHO;
  tcsetattr(0, TCSANOW, &term);
  {
    FILE *f;
    f = fopen("forth.bin", "rb");
    fread(ram, 1, sizeof(ram), f);
    fclose(f);
  }
  for (;;) {
    /*
      fprintf(stderr, "[DEBUG\n");
      fprintf(stderr, "  R0=%.6o R1=%.6o R2=%.6o R3=%.6o\n", machine.r[0], machine.r[1], machine.r[2], machine.r[3]);
      fprintf(stderr, "  R4=%.6o R5=%.6o R6=%.6o R7=%.6o]\n\n", machine.r[4], machine.r[5], machine.r[6], machine.r[7]);
    */
    step();
  }
}
#endif
