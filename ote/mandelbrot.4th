1 1 cells 2* 2* 3 + lshift constant precision
160 constant screen-width
80 constant screen-height
precision 2* negate constant mandel-left
precision 2/ constant mandel-right
precision negate constant mandel-bottom
precision constant mandel-top
mandel-right mandel-left - constant mandel-width
mandel-top mandel-bottom - constant mandel-height
: scale-x   mandel-width screen-width */ mandel-left + ;
: scale-y   mandel-height screen-height */ mandel-bottom + ;
: scale   scale-x >r scale-y r> ;
: render   if ." #" else space then ;
: ;] ; immediate
: ['']   bl word find drop postpone literal ; immediate
: [compile]'   dup [''] ;] = if drop 1 rdrop exit then compile, ;
: compile'   postpone literal postpone compile, ;
: [postpone]   1+ if [compile]' else compile' then 0 ;
: [:   : begin bl word find [postpone] until postpone ; immediate ; immediate
: *'   precision */ ;
: 2^   dup *' ;
: 2^'   2dup 2^ swap 2^ - >r *' 2* r> ;
: +'   rot + >r + r> ;
: iterate   2swap 2>r 2^' 2r@ +' 2r> 2swap ;
: escaped?   abs swap abs max 2 precision * >= ;
[: ?iterate   iterate 2dup escaped? if leave then ;]
: (mandelbrot)   0 0 64 0 do ?iterate loop 2nip escaped? invert ;
: mandelbrot   cr screen-height 0 do
                 screen-width 0 do
                   j i scale (mandelbrot) render
                 loop cr
               loop ;
mandelbrot bye
