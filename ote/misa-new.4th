4 chars buffer: queued   variable #queued
: s,   over + swap ?do i c@ c, loop ;
: 2!   >r dup r@ c! 8 rshift r> 1+ c! ;
: 2,   dup c, 8 rshift c, ;   : 2B,   dup 8 rshift c, c, ;
: queue   #queued @ queued + 2! 2 #queued +! ;
: unqueue   queued #queued @ s, 0 #queued ! ;
: mode   create dup 2* 2* 2* c, 1+ does> c@ or ;
1 mode ) mode )+ mode -) drop   : #   queue 7 )+ ;
: 1op   create dup c, 1+ does> c@ 5 lshift or c, unqueue ;
0 1op inc, 1op dec, 1op not, 1op rsh, 1op push, 1op pop, drop
: 2merge   10 lshift >r 5 lshift or r> or ;
: 2op   create dup c, 1+ does> c@ 48 + 2merge 2B, unqueue ;
0 2op mov, 4 + 2op call, 2op cmp, 2op add, 2op sub, 2op and,
2op or, 2op xor, 2op setcc, drop
: s:   create dup c, 1+ does> c@ ;   0 s: =, s: <>, s: <, s: >,
drop variable (org)    : org   here swap - (org) ! ;
: cmov,   49 + 2merge 2B, unqueue ;   : j,   7 swap cmov, ;
: (maybe)   7 )+ swap j, ;   : (ever)   7 )+ 7 mov, ;
: (jump)   here 0 2, ;   : $   here (org) @ - ;
: (land)   $ swap 2! ;   : (back)   2! ;   : jmp,   7 mov, ;
: ahead,   (ever) (jump) ;   : -if,   (maybe) (jump) ;
: then,   (land) ;   : else,   (ever) (jump) swap (land) ;
: begin,   $ ;   : again,   (ever) (jump) (back) ;
: -until,   (maybe) (jump) (back) ;   : unext, 7 -) dup cmp, ;
: -while,   (maybe) (jump) swap ;   : exit,   6 )+ jmp, ;
: repeat,   (ever) (jump) (back) (land) ;
: label:   create -1 , does> @ # 6 -) call, ;
: ''   ' >body @ ;   : ::   $ ' >body ! ;
: (reserve)   $ swap allot ;   : (forward)   $ - allot ;
: (back)   0 (reserve) swap (forward) ;   : +f   8 or ;
0 s: nz s: z s: nc s: c s: nznc s: nzc s: znc s: zc drop
