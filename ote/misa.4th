0 constant r0   1 constant r1   2 constant r2   3 constant r3
4 constant r4   5 constant r5   6 constant r6   7 constant r7
create queued 4 allot   variable #queued
: s,   over + swap ?do i c@ c, loop ;
: 2!   >r dup r@ c! 8 rshift r> 1+ c! ;
: 2,   dup c, 8 rshift c, ;   : 2B,   dup 8 rshift c, c, ;
: queue   #queued @ queued + 2! 2 #queued +! ;
: unqueue   queued #queued @ s, 0 #queued ! ;
: mode   create dup c, 1+ does> c@ 2* 2* 2* or ;
1 mode @) mode @+) mode @-) drop   : #   queue r7 @+) ;
: 1op   create dup c, 1+ does> c@ 5 lshift or c, unqueue ;
0 1op inc, 1op dec, 1op neg, 1op not, 1op lsh, 1op rsh, drop
: 2merge   10 lshift >r 5 lshift or r> or ;
: 2op   create dup c, 1+ does> c@ 48 + 2merge 2B, unqueue ;
0 2op mov, 2op cmoveq, 2op cmovlt, 2op cmovgt, 2op call,
2op cmp, 2op add, 2op sub, 2op and, 2op or, 2op xor, drop
0 constant =,   1 constant <,   2 constant >,   variable (org)
: org   here swap - (org) ! ;   : jmp,   r7 mov, ;
: cmov,   49 + 2merge 2B, unqueue ;   : jcc,   r7 swap cmov, ;
: (maybe)   r7 @+) swap jcc, ;   : (ever)   r7 @+) jmp, ;
: (jump)   here 0 2, ;   : $   here (org) @ - ;
: (land)   $ swap 2! ;   : (back)   2! ;
: ahead,   (ever) (jump) ;   : if,   (maybe) (jump) ;
: then,   (land) ;   : else,   (ever) (jump) swap (land) ;
: begin,   $ ;   : again,   (ever) (jump) (back) ;
: until,   (maybe) (jump) (back) ;   : unext, r7 @-) dup cmp, ;
: while,   (maybe) (jump) swap ;   : exit,   r6 @+) jmp, ;
: repeat,   (ever) (jump) (back) (land) ;
: label:   create -1 , does> @ # r6 @-) call, ;
: ''   ' >body @ ;   : ::   $ ' >body ! ;
: (reserve)   $ swap allot ;   : (forward)   $ - allot ;
: (back)   0 (reserve) swap (forward) ;
