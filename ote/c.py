from dataclasses import dataclass

from collections.abc import Callable, Iterator
from typing import TypeVar

from random import seed, randint, random
from math import sin, cos, tau

seed(0)

def random_color() -> str:
  red: int = randint(0, 255)
  green: int = randint(0, 255)
  blue: int = randint(0, 255)
  return f"#{red:02x}{green:02x}{blue:02x}"

@dataclass

class Point:
  color: str
  radius: float
  phase: float
  factor: int
  blur_amp: float
  blur_center: float
  blur_phase: float
  blur_factor: int

def random_point() -> Point:
  color: str = random_color()
  radius: float = (random() + 1)
  phase: float = random() * tau
  factor: int = randint(1, 16)
  blur_amp: float = random() / 2
  blur_center: float = random() * (1 - blur_amp * 2) + blur_amp
  blur_phase: float = random() * tau
  blur_factor: int = randint(1, 4)
  return Point(color, radius, phase, factor, blur_amp, blur_center, blur_phase, blur_factor)

def iterate_point(p: Point) -> Point:
  color: str = p.color
  radius: float = p.radius
  phase: float = (p.phase + (tau * p.factor)/100.0) % tau
  factor: int = p.factor
  blur_amp: float = p.blur_amp
  blur_center: float = p.blur_center
  blur_phase: float = (p.blur_phase + (tau * p.blur_factor)/100.0) % tau
  blur_factor: int = p.blur_factor
  return Point(color, radius, phase, factor, blur_amp, blur_center, blur_phase, blur_factor)

def iterate_points(ps: list[Point]) -> list[Point]:
  return [iterate_point(p) for p in ps]

def render_point(p: Point) -> str:
  xpos: float = cos(p.phase) * p.radius
  ypos: float = sin(p.phase) * p.radius
  blur: float = (sin(p.blur_phase) * p.blur_amp + p.blur_center) / 16
  color: str = p.color
  return f"{xpos}em {ypos}em {blur}em {color}"

def render_points(ps: list[Point]) -> str:
  return ", ".join([render_point(p) for p in ps])

T = TypeVar("T")
def markovish(f: Callable[[T], T], t: T, n: int) -> Iterator[T]:
  for _ in range(n):
    yield t
    t = f(t)

points: list[Point] = [random_point() for _ in range(16)]
renderings: list[str] = list(map(render_points, markovish(iterate_points, points, 100)))
print("@keyframes schizopolitics {")
for i, rendering in enumerate(renderings):
  print(f"  {i}% {{ text-shadow: {rendering}; }}")
print("  100% {{ text-shadow: {}; }}".format(render_points(points)))
print("}")
