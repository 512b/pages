s" misa-new.4th" included 8 base !
0 constant w   4 constant i   5 constant s   6 constant r
: next,   i )+ w mov,   w )+ jmp, ;

177776 constant io
10000 constant s0'
20000 constant r0'

label: docol'   label: docode'   label: dodoes'   label: docon'
label: dovar'

label: lit'   label: branch'   label: 0branch'

label: dup'   label: drop'   label: swap'   label: rot'
label: over'   label: >r'   label: r>'   label: r@'

label: 0='   label: ='   label: <'   label: >'

label: #in'   label: >in'   label: word'
label: source'   label: refill'   label: evaluate'
label: type'

label: quit'   label: key'   label: emit'

label: cold-start'

: ['']   ' >body postpone literal postpone @ ; immediate
: with'   '' :: 2, ;
: code'   :: [''] docode' 2, ;
: :'   :: [''] docol' 2, ;
: begin'   $ ;   : again'   [''] branch' 2, 2, ;
: until'   [''] 0branch' 2, 2, ;
: dbg,    176000 or 2B, ;

here 0 org
  s0' # s mov,
  r0' # r mov,
  10 (reserve)
  :: docol'   i r -) mov,   w i mov,   next,
  :: docode'   w jmp,
  :: dodoes'   i r -) mov,   w )+ i mov,   w s -) mov,   next,
  :: docon'   w ) s -) mov,   next,
  :: dovar'   w s -) mov,   next,
  with' dovar' #in'
  with' dovar' >in'
  code' lit'   i )+ s -) mov,   next,
  code' key'   io # w mov,   w ) s -) mov,   next,
  code' emit'   io # w mov,   s )+ w ) mov,   next,
  code' 0branch'   0 # s )+ cmp,   '' branch' 2 + # =, j,
                   i )+ w mov,   next,
  code' dup'   s ) push,   next,
  code' drop'   s )+ w mov,   next,
  code' swap'   s )+ 1 mov,   s ) 0 mov,
                1 s ) mov,   0 s -) mov,   next,
  code' rot'   s )+ 2 mov,   s )+ 1 mov,   s ) 0 mov,
                1 s ) mov,   2 s -) mov,   0 s -) mov,   next,
  code' over'   s w mov,   w w )+ cmp,   w ) s -) mov,   next,
  code' >r'   s )+ r -) mov,   next,
  code' r>'   r )+ s -) mov,   next,
  code' r@'   r ) s -) mov,   next,
  code' 0='   0 # s ) cmp,   z +f s ) setcc,   next,
  code' ='   s )+ s ) cmp,   z +f s ) setcc,   next,
  code' <'   s )+ s ) cmp,   c +f s ) setcc,   next,
  code' >'   s )+ s ) cmp,   nznc +f s ) setcc,   next,
  code' branch'   i ) i mov,   next,
  code' refill'   io # 1 mov,   r0' # 2 mov,   begin,
                  1 ) 0 mov,   0 1 ) mov,   0 2 )+ mov,   2 dec,
                  10 # w cmp,   <>, -if,   2 dec,   2 dec,   then,
                  12 # w cmp,   <>, -until,
                  r0' 1 + # 2 sub,
                  '' #in' 2 + # 1 mov,   2 1 ) mov,   next,
  code' source'   r0' # s -) mov,   '' #in' 2 + # w mov,
                  w ) s -) mov,   next,
  code' type'   s )+ 0 mov,   s )+ 1 mov,   io # 2 mov,
                begin,   0 # 0 cmp,   <>, -if,   next,   then,
                1 )+ 2 ) mov,   1 dec,   0 dec,   again,
  :' quit'   begin'   '' refill' 2,   '' source' 2,   '' type' 2,   again'
  :: cold-start'   '' quit' 2,
  (back)
  '' cold-start' # i mov,
  next,
  (forward)
dup here swap - type bye
( 345678901234567890123456789012345678901234567890123456789012 )
