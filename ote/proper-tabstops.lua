-- mod-version:3
-- plugin by @otesunki
-- licensed under AGPLv3 for Now.

local core = require "core"
local common = require "core.common"
local style = require "core.style"
local StatusView = require "core.statusview"
local CommandView = require "core.commandview"
local DocView = require "core.docview"
local Doc = require "core.doc"
local keymap = require "core.keymap"

local function next_stop(source, location, modulus)
	local new_location = location - source + modulus
	new_location = new_location - (new_location % modulus) + source
	return new_location
end

local draw_line_text = DocView.draw_line_text
function DocView:draw_line_text(line, x, y)
	if getmetatable(self) ~= DocView then
		return draw_line_text(self, line, x, y)
	end
	local default_font = self:get_font()
	local tx, ty = x, y + self:get_line_text_y_offset()
	local last_token = nil
	local tokens = self.doc.highlighter:get_line(line).tokens
	local tokens_count = #tokens
	local _, indent_size = self.doc:get_indent_info()
	default_font:set_tab_size(indent_size)
	local tabstopmod = default_font:get_width(" ") * indent_size
	if string.sub(tokens[tokens_count], -1) == "\n" then
		last_token = tokens_count - 1
	end
	for tidx, type, text in self.doc.highlighter:each_token(line) do
		local color = style.syntax[type]
		local font = style.syntax_fonts[type] or default_font
		if tidx == last_token then text = text:sub(1, -2) end
		if string.find(text, "\t") then
			for char in common.utf8_chars(text) do
				if char == "\t" then
					tx = next_stop(x, tx, tabstopmod)
				else
					tx = renderer.draw_text(font, char, tx, ty, color)
				end
			end
		else
			tx = renderer.draw_text(font, text, tx, ty, color)
		end
		if tx > self.position.x + self.size.x then break end
	end
	return self:get_line_height()
end

local get_col_x_offset = DocView.get_col_x_offset
function DocView:get_col_x_offset(line, col)
	if getmetatable(self) ~= DocView then
		return get_col_x_offset(self, line, col)
	end
	local default_font = self:get_font()
	local _, indent_size = self.doc:get_indent_info()
	default_font:set_tab_size(indent_size)
	local column = 1
	local xoffset = 0
	local tabstopmod = default_font:get_width(" ") * indent_size
	for _, type, text in self.doc.highlighter:each_token(line) do
		local length = #text
		local font = style.syntax_fonts[type] or default_font
		if font ~= default_font then font:set_tab_size(indent_size) end
		for char in common.utf8_chars(text) do
			if column >= col then
				return xoffset
			end
			if char == "\t" then
				xoffset = next_stop(0, xoffset, tabstopmod)
			else
				xoffset = xoffset + font:get_width(char)
			end
			column = column + #char
		end
	end

	return xoffset
end

local get_x_offset_col = DocView.get_x_offset_col
function DocView:get_x_offset_col(line, x)
	if getmetatable(self) ~= DocView then
		return get_x_offset_col(self, x)
	end

	local line_text = self.doc.lines[line]

	local xoffset, last_i, i = 0, 1, 1
	local default_font = self:get_font()
	local _, indent_size = self.doc:get_indent_info()
	default_font:set_tab_size(indent_size)
	local tabstopmod = default_font:get_width(" ") * indent_size
	for _, type, text in self.doc.highlighter:each_token(line) do
		local font = style.syntax_fonts[type] or default_font
		if font ~= default_font then font:set_tab_size(indent_size) end
		local width = font:get_width(text)
		for char in common.utf8_chars(text) do
			local w = font:get_width(char)
			if xoffset >= x then
				return (xoffset - x > w / 2) and last_i or i
			end
			if char == "\t" then
				xoffset = next_stop(0, xoffset, tabstopmod)
			else
				xoffset = xoffset + w
			end
			last_i = i
			i = i + #char
		end
	end

	return #line_text
end
